import AbstractServiceProvider from "./AbstractServiceProvider";
import EventDispatcher from "../dispatcher/EventDispatcher";
import Request from "../protocol/request/Request";
import Response from "../protocol/response/Response";
import Artix from "../Artix";
import Kernel from "../kernel/Kernel";
import {EventRequest} from "../event/EventRequest";

class ProtocolServiceProvider extends AbstractServiceProvider {
    registration(App) {
        App['core.dispatcher'] = new EventDispatcher();

        /**
         * @param {Request} request
         * @param {Response} response
         * */
        App['core.protocol.request'] = (request, response) => {
            App['core.dispatcher'].dispatch(Kernel.REQUEST(), new EventRequest(request, response));
            response.render('<h1>Fuck</h1>');
        }
    }

    /**
     * @param {Artix} App
     * */
    boot(App) {
        App['core.protocol'].init(App['core.protocol.request']);
    }
}

export {ProtocolServiceProvider}