import AbstractServiceProvider from "./AbstractServiceProvider";
import {Logger, LoggerDefault, LoggerInfo} from "./logger/Logger";
import Kernel from "../kernel/Kernel";
import {EventRequest} from "../event/EventRequest";

export default class LoggerServiceProvider extends AbstractServiceProvider {
    registration(App) {
        App['core.log'] = (message, level = LoggerDefault) => {
            if (App['core.debug']) {
                new Logger(message, level);
            }
        }
    }

    subscribe(App, EventDispatcher) {
        EventDispatcher.addEventListener(Kernel.REQUEST(), (event) => {
            let request = event.getRequest();
            App['core.log'](`Server: [${request.getMethod()}] ${request.getUrl()}`, LoggerInfo);
        })
    }
}