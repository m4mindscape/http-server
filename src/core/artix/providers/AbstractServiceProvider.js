import Artix from "../Artix";
import EventDispatcher from "../dispatcher/EventDispatcher";
import Container from "../container/Container";

export default class AbstractServiceProvider {
    constructor(params) {
        if (params) {
            this.params = params;
        }
    }

    /**
     * @param {Artix|Container} App
     * */
    registration(App) {

    }

    /**
     * @param {Artix|Container} App
     * */
    boot(App) {

    }

    /**
     * @param {Artix|Container} App
     * @param {EventDispatcher} EventDispatcher
     * */
    subscribe(App, EventDispatcher) {

    }

    static getProviderName() {
        return `Provider:${this.name}`;
    }
}