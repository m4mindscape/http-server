let LoggerError = '\x1b[31m';
let LoggerWarning = '\x1b[33m';
let LoggerInfo = '\x1b[34m';
let LoggerDefault = '\x1b[37m';

class Logger {
    /**
     * @param {string} message
     * @param {string} level
     * */
    constructor(message, level) {
        console.log(level+message);
    }
}

export {Logger, LoggerError, LoggerDefault, LoggerInfo, LoggerWarning};