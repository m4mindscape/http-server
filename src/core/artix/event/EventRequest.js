import Request from "../protocol/request/Request";
import Response from "../protocol/response/Response";

class EventRequest {

    /**
     * @param {Request} request
     * @param {Response} response
     * */
    constructor(request, response) {
        this.request = request;
        this.response = response;
    }

    getRequest() {
        return this.request;
    }

    getResponse() {
        return this.response;
    }
}

export {EventRequest}