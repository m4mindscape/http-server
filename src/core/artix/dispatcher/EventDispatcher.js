const listeners = new Map();
export default class EventDispatcher {
    constructor () {

    }

    dispatch(EventName, Event) {
        listeners.get(EventName).forEach(value => {
            value(Event);
        })
    }

    /**
     * @param {string} EventName
     * @param {function} callback
     * */
    addEventListener(EventName, callback) {
        if (!listeners.has(EventName)) {
            listeners.set(EventName, []);
        }

        listeners.get(EventName).push(callback);
    }
}