import Container from "./container/Container";
import LoggerServiceProvider from "./providers/LoggerServiceProvider";
import {ProtocolServiceProvider} from "./providers/ProtocolServiceProvider";
import {LoggerInfo} from "./providers/logger/Logger";
import HTTP from "./protocol/http/HTTP";

let booted = false;

export default class Artix extends Container {
    constructor() {
        super();
        this['core.debug'] = true;
        this['core.protocol'] = new HTTP(3000);

        this.register(LoggerServiceProvider);
        this.register(ProtocolServiceProvider);

        return this;
    }

    run() {
        if (!booted) {
            this.boot();
        }
    }

    boot() {
        if (booted) {
            return;
        }

        booted = true;

        this.getProviders().forEach((value, key) => {
            value.subscribe(this, this['core.dispatcher']);
            this['core.log'](`Subscribe provider [${key}]`, LoggerInfo);
            value.boot(this);
            this['core.log'](`Booted provider [${key}]`, LoggerInfo);
        });
    }
}