export default class Request {
    constructor(request) {
        this.request = request;
    }

    getMethod() {
        return this.request.method;
    }

    getUrl() {
        return this.request.url;
    }
}