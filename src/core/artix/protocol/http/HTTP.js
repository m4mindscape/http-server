import http from 'http';
import Request from "../request/Request";
import Response from "../response/Response";

export default class HTTP {
    /**
     * @param {number} port
     * @param {string?} host
     * */
    constructor(port, host) {
        this.port = port;
        this.host = host;
        this.http = null;
    }

    init(callback) {
        this.http = http.createServer((req, res) => {
            req.on('error', (err) => {
                console.dir('err');
            }).on('data', (data) => {
                console.dir('data');
            }).on('end', () => {
                callback(new Request(req), new Response(res));
            }).on('request', () => {
                console.dir('request');
            }).on('connect', () => {
                console.dir('connect');
            })
        });

        this.http.listen({
            port: this.port,
            host: this.host
        })
    }
}