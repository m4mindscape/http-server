export default class Response {
    constructor(response) {
        this.response = response;
    }

    render(data) {
        this.response.end(data);
    }
}