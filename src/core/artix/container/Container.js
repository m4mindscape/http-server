import AbstractServiceProvider from "../providers/AbstractServiceProvider";
import {LoggerInfo, LoggerWarning} from "../providers/logger/Logger";

const _values = new Map();
const _providers = new Map();

export default class Container {
    /**
     * @param {AbstractServiceProvider} provider
     * @param {object?} params
     * */
    register(provider, params) {
        if (typeof provider === 'function') {
            provider = new provider(params);
        }

        if (!_providers.has(provider.constructor.getProviderName())) {
            provider.registration(this);
            _providers.set(provider.constructor.getProviderName(), provider);
            _values.set(provider.constructor.getProviderName(), params);

            if (this['core.log']) {
                this['core.log'](`Registration provider [${provider.constructor.getProviderName()}]`, LoggerInfo);
            }
            return;
        }

        if (this['core.log']) {
            this['core.log'](`Provider [${provider.constructor.getProviderName()}] already registered`, LoggerWarning);
        }
    }

    getProviders() {
        return _providers;
    }

    getProvider(key) {
        return _providers.get(key);
    }

    getValues() {
        return _values;
    }

    getValue(key) {
        return _values.get(key);
    }
}