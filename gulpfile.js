let gulp = require('gulp');
let console = require('child_process').spawn;

gulp.task('run-server', cb => {
    let run = console('powershell.exe', ['npm run dev'], {stdio: 'inherit'});

    run.on('data', data => {
        console.log(data);
    });
    cb();
});


gulp.task('dev', gulp.series('run-server'));